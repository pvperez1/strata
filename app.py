from flask import Flask, jsonify, request, url_for, redirect, session, render_template, g
from flaskext.mysql import MySQL
from werkzeug.security import generate_password_hash, check_password_hash
import os

app = Flask(__name__)
app.config['SECRET_KEY'] = os.urandom(24)


app.config['DEBUG'] = True

# MySQL configurations

app.config['MYSQL_DATABASE_USER'] = 'pvperez1'
app.config['MYSQL_DATABASE_PASSWORD'] = 'aU76GsWd2DMZ4Nd'
app.config['MYSQL_DATABASE_DB'] = 'pvperez1$strata'
app.config['MYSQL_DATABASE_HOST'] = 'pvperez1.mysql.pythonanywhere-services.com'
'''
app.config['MYSQL_DATABASE_USER'] = 'root'
app.config['MYSQL_DATABASE_PASSWORD'] = ''
app.config['MYSQL_DATABASE_DB'] = 'strata'
app.config['MYSQL_DATABASE_HOST'] = 'localhost'
'''
flaskmysql = MySQL()
flaskmysql.init_app(app)

def connect_db(flaskmysql):
    con = flaskmysql.connect()
    return con

def get_db(flaskmysql):
    if not hasattr(g, 'mysql_db'):
        g.mysql_db = connect_db(flaskmysql)
    return g.mysql_db

# define a common function for retrieving user
def get_current_user():
    user_result = None

    #check if user is in session
    if 'user' in session:
        user = session['user']

        #retrieve user info from database
        con = get_db(flaskmysql)
        cur = con.cursor()
        cur.execute('select id, name, password, admin from users where name = %s', [user])
        user_result = cur.fetchone()

    return user_result

@app.teardown_appcontext
def close_db(error):
    if hasattr(g, 'mysql_db'):
        g.mysql_db.close()

@app.route('/', methods=['POST','GET'])
def index():
    user = get_current_user()

    if not user: # Send User to Landing Page if not logged in
        return render_template('strata.html')

    alert_message = request.args.get('alert_message')

    if request.method == 'POST': # this is true when Raise Issue button is clicked
        issue_description = request.form['issue_description']
        # Insert to Database
        con = get_db(flaskmysql)
        cur = con.cursor()
        cur.execute('insert into issues (issue_description) values (%s)', (issue_description))
        con.commit()
        alert_message = "alert-success"

    # Read to Database
    con = get_db(flaskmysql)
    cur = con.cursor()
    cur.execute('select * from issues')
    results = cur.fetchall()
    issue_count = 0

    # Start - Conversion from Tuples to Dictionary (Copied from Stack Overflow =))
    def Convert(tup, di):
        di = dict(tup)
        return di

    dictionary = {}
    results_dict = Convert(results, dictionary)
    # return max key
    issue_count = int(max(results_dict.keys()))
    # get name
    user_name = user[1]
    user_name = user_name.split('@')[0].capitalize()

    return render_template('base.html', results_dict=results_dict, issue_count=issue_count, alert_message=alert_message, user=user, user_name=user_name)

@app.route('/issue_update', methods=['POST','GET'])
def issue_update():
    issue_num = request.args.get('edit')


    user = get_current_user()

    if request.method == 'POST': #initially this is not true, only tru when update button is clicked
        issue_description = request.form['issue_description']
        issue_num = request.form['issue_number']
        if request.form['form_button'] == 'delete': # perform delete and redirect to homepage
            con = get_db(flaskmysql)
            cur = con.cursor()
            cur.execute('delete from issues where issue_number = %s', (issue_num))
            con.commit()
            print("deleted")
            alert_message = "alert-danger"
            return redirect(url_for('index',alert_message=alert_message))

        elif request.form['form_button'] == 'update': # Update database and redirect to homepage
            con = get_db(flaskmysql)
            cur = con.cursor()
            cur.execute('update issues \
                                set issue_description = %s \
                              where issue_number = %s', (issue_description,issue_num))
            con.commit()
            print("edited")
            alert_message = "alert-warning"
            return redirect(url_for('index',alert_message=alert_message))


    if not issue_num:  # user tried to access /issue_update via address bar
        return redirect(url_for('index'))

    # Read to Database Specific Record
    con = get_db(flaskmysql)
    cur = con.cursor()
    cur.execute('select * from issues where issue_number = %s', issue_num)
    results = cur.fetchall()
    #print(results)

    # Start - Conversion from Tuples to Dictionary (Copied from Stack Overflow =))
    def Convert(tup, di):
        di = dict(tup)
        return di

    dictionary = {}
    results_dict = Convert(results, dictionary)
    # print('results_dict : ', results_dict)

    return render_template('issue_update.html', results_dict=results_dict, user=user)

@app.route('/signup', methods=['GET','POST'])
def signup():
    user = get_current_user()

    if user: #if user is logged in, redirect to index
        return redirect(url_for('index'))

    if request.method == 'POST': # true when sign up button is clicked
        con = get_db(flaskmysql)
        cur = con.cursor()
        hashed_password = generate_password_hash(request.form['password'], method='sha256')

        # check first if email already exists
        cur.execute('select name from users where name = %s', (request.form['email']))
        user_result = cur.fetchone()
        if user_result: # this means user already exists
            alert_message = "alert-danger"
            return render_template('signup.html', user=user, alert_message=alert_message)


        # Insert to Database
        cur.execute('insert into users (name, password, admin) values (%s,%s,%s)', (request.form['email'], hashed_password,'0'))
        con.commit()
        session['user'] = request.form['email']

        return redirect(url_for('index'))
    return render_template('signup.html', user=user)


@app.route('/signin', methods=['GET','POST'])
def signin():
    user = get_current_user()

    if user: #if user is logged in, redirect to index
        return redirect(url_for('index'))

    if request.method == 'POST': # true when sign in button is clicked
        # Read to Database Specific Record
        con = get_db(flaskmysql)
        cur = con.cursor()

        name = request.form['email']
        password = request.form['password']

        cur.execute('select id, name, password from users where name = %s', (name))
        user_result = cur.fetchone()

        if not user_result:
            alert_message = "alert-danger"
            return render_template('signin.html', user=user, alert_message=alert_message)
        elif check_password_hash(user_result[2],password):
            session['user'] = user_result[1]
            return redirect(url_for('index'))
        else:
            alert_message = "alert-danger"
            return render_template('signin.html', user=user, alert_message=alert_message)

    return render_template('signin.html', user=user)

@app.route('/signout')
def signout():
    session.pop('user', None)
    return redirect(url_for('index'))

if __name__ == '__main__':
    app.run()